# DevCom Contribution Guidelines

## Getting Involved

There are many ways you can contribute to the various DevCom projects. Examples might include:

- Fixing or adding documentation
- Adding features that make projects accessible to more teams
- Improving test coverage

DevCom is meant to be an open and welcoming community to anyone that wishes to contribute. Before getting involved, be sure to read through the principles outlined within the [DevCom Code of Conduct](./CODE_OF_CONDUCT.md).

In most cases, the contribution process will follow these steps:

1. Create a GitLab issue to report an issue or request a feature
2. Fork the repo and perform work
3. Create a merge request to allow code review
4. Code contributions are merged and, if applicable, deployed


## Reporting Issues and Requesting Features

All DevCom-sponsored projects make use of GitLab's issues and issue boards for project management. This is to provide consistency and transparency for all work being done within the community. You are welcome to create new issues, comment on existing issues to add additional context, or up/down vote issues.

A few things to keep in mind:

- **If your contribution is minor** (such as fixing typos), feel free to open a merge request with the suggested change.
- **If your contribution is major** (such as a new feature), open an issue _first_ to discuss your idea before spending a lot of time on something that may or may not be accepted as you originally envisioned.
- **Search first.** Be mindful of people's time by searching for relevant issues and merge requests before creating anything new. It's generally better to join existing conversations than starting a duplicate one.
- **Use reactions, not +1 comments.** Instead of adding "+1" comments, use the emoji reactions to up/down vote an issue. This keeps conversations focused and prevents the amount of notification spam sent to contributors.
- **Use the templates.** Most projects will have an issue template to help you think about the details you need to include in your issue. Try to use the one that closest matches the type of issue you are creating. If none match, feel free to start from scratch.


### Reporting Security Issues

All security issues should be disclosed responsibly by doing either of the following:

- Create a [confidential issue](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html) on the project by creating an issue and checking the **This issue is confidential** checkbox.
- [Contact the IT Security Office](https://security.vt.edu/about/contact.html) with appropriate details. If including attack vectors or proofs of concept, be sure to encrypt the message using the PGP key referenced on their site.

After reporting, do not publicly disclose the issue until it has been acknowledged and resolved.


## Making Contributions

When you're ready to make contributions, please use the following guidelines:

- **Work in a forked repo.** All DevCom projects use a [project forking workflow](https://docs.gitlab.com/ee/workflow/forking_workflow.html), which means you make your changes in your own copy of the repo and then send a merge request to merge them upstream.
- **Start your work from master.** The master branch reflects current production-grade code. In most cases, your new contribution should be based from master. If you feel you should use another branch, but aren't sure, ask a maintainer!
- **Keep contributions focused.** If, while working on something, you notice something else that needs to be fixed, keep focused and finish the original contribution. Then, branch off of master again and do your next contribution. Doing so keeps merge requests tight, focused, and easier to review.
- **Write test cases, when possible.** If you're fixing a bug, write a test case that first proves the bug. Then, write the code to fix the bug to get the test to pass. This helps reduce regressions with future updates.
- **Follow formatting guidelines.** Most projects will have formatting guidelines appropriate to the programming language. Be sure you are following them to reduce the number of "spaces converted to tabs" commits and make reviewing easier.
- **Write informative git commit messages.** Messages such as "did something" don't help reviewers understand what you did. [This article](https://chris.beams.io/posts/git-commit/) contains several best practices for Git commit messages.
- **Sign your code.** While not required, it is _strongly_ encourage that you sign your code. [A Git Horror Story](https://mikegerwitz.com/2012/05/a-git-horror-story-repository-integrity-with-signed-commits) and [Sign your Commits or Else!](https://blog.mikesir87.io/2018/05/sign-your-commits-or-else/) can explain why.


## Creating Merge Requests

All contributions to projects come through merge requests. To help facilitate the process, the following guidelines have been established:

- **Ensure builds are passing.** Before creating the merge request, if your project has builds, ensure they are running and passing. The deploy stages are expected to fail, but all build and test stages should be green!
- **Explain the purpose clearly.** When creating a merge request, answer the question of "If this is merged, this code will... what?" For projects that have templates, this will be easier to follow. Help make it easier for reviewers to understand why your code should be accepted.
- **All code is reviewed.** No one is excluded, even maintainers on specific projects. Fully expect to have your code reviewed, nit-picked, and analyzed. Work with the reviewers in a professional manner to work through issues and comments.


