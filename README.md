# DevCom Guidelines

- [Code of Conduct](./CODE_OF_CONDUCT.md) - outlines behavioral expectations for all individuals interested in participating in DevCom activities
- [Contribution Guidelines](./CONTRIBUTING.md) - contains guidelines and expectations for contributing to DevCom projects


